module Chopps
  module UserHelpers
    extend self

    def get_user(user, params)
      bar = Bar.find_bar_near(params)
      user_has_pkg = false
      bar_json = nil
      if not bar.nil? then
        user_has_pkg = user_has_pkg(current_user, bar.id)
        bar_json = { "name" => bar.name, "id" => bar.id.to_s}
      end
  
      respond_to do |format|
        format.json { render :json => {:success => true, 
                                  :user => current_user.as_json(reg_info: true, home_info: true),
                                  :nr_bars => Bar.count, :bar => bar_json, :user_has_pkg => user_has_pkg,
                                  :chopp_types => MainYetting.CHOPPS_TYPE
                                } }
      end
    end
    
    
    def user_has_pkg(current_user, bar_id) 
      if not current_user.package_users.nil? then 
         current_user.package_users.each do |pkg_user|
           if pkg_user.bar_id == bar_id then
             return true
           end
         end 
       end
       return false
    end

  end
  
  
  
end