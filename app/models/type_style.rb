class TypeStyle
  
  include Mongoid::Document

  field :_id,                                  type: Integer
  field :d,     as: :type_style,               type: String # Tipo/Estilo (Ales-Porter | Larges-Book)

end