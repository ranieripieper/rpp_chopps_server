require 'geocoder'

class Address
  include Mongoid::Document
  #include Mongoid::Spacial::Document
  include Geocoder::Model::Mongoid
  
  field :_id, type: String, default: nil
  field :street, type: String
  field :number, type: String
  field :complement, type: String
  field :neighborhood, type: String
  field :zip_code, type: String
  field :city, type: String
  field :state, type: String
  #field :location, type: Array, spacial: {lat: :latitude, lng: :longitude, return_array: true }
  
  field :coordinates, :type => Array
  field :address
  
  field :id
  
  def id
    _id.to_s
  end
  
  embedded_in :addressed, polymorphic: true

  geocoded_by :address
  
  after_validation :geocode

  def serializable_hash(options)
      original_hash = super(options)
      Hash[original_hash.map {|k, v| [self.aliased_fields.invert[k] || k , v] }]
  end
  
  def address
    [street, number, city, state, 'Brasil'].compact.join(', ')
  end

  geocoded_by :address, :coordinates => :coords 
  reverse_geocoded_by :coordinates, :address => :loc 

end