class Package

  include Mongoid::Document
  include Mongoid::Timestamps

  before_save :calculate_values
  before_create :assign_id
  
  field :_id,                                  type: Integer
  field :q,     as: :qtde,                     type: Integer, default: 10 #quantidade de unidades do pacote a serem vendidas
  field :qv,    as: :qtde_vend,                type: Integer, default: 0 #quantidade de unidades do pacote vendidas
  field :a,     as: :active,                   type: Boolean, default: true
  field :t,     as: :type,                     type: Integer, default: 1 # Tipo (1. Chopp ou 2. Cerveja) - MainYetting.PACKAGE_TYPE
  field :dt,    as: :dt_sale_limit,            type: Date #data de validade para venda do pacote
  field :v,     as: :sale_value,               type: Float #valor de venda do pacote (soma o valor dos itens)
  field :rv,    as: :real_value,               type: Float #valor real do pacote - sem desconto (soma o valor dos itens)

  # o bar pode escolher se o consumo do pacote terá uma data limite ou será somado dias no ato da compra
  field :d_c,   as: :qt_days_consumption,      type: Integer, default: 90 # quantidade de dias que o pacote expira após a compra
  field :dt_c,  as: :dt_consumption,           type: Date #data limite para consumo -

  belongs_to :bar
  belongs_to :package_user

  embeds_many :package_items

  def to_param
    id.to_s
  end

#  accepts_nested_attributes_for :package_items

  #default_scope -> { where(active: true) }

  scope :search_value, ->(value){ where(:v.lte => value) }
  scope :admin_scope, -> { order_by(:active.desc) }
  
  def as_json(options = { })
    options = options.merge(base_info())
    h = super(options)

    ##calcula data de consumo 
    if dt_consumption.nil? and not qt_days_consumption.nil? then
      h[:dt_consumption] = Date.today() + qt_days_consumption.to_i
    end
    
    h
  end
  
  def as_json1(options = { })
    puts ">>>> packages as_json"
    if not options[:base_info].nil? and options[:base_info] then
      options = options.merge(base_info())
    elsif not options[:detail_info].nil? and options[:detail_info] then
      options = options.merge(detail_info())
    elsif not options[:search_package_info].nil? and options[:search_package_info] then
      options = options.merge(search_package_info())
    elsif not options[:home_info].nil? and options[:home_info] then
      options = options.merge(home_info())
    elsif not options[:user_packages_info].nil? and options[:user_packages_info] then
      options = options.merge(user_packages_info())
    elsif not options[:user_buy_package].nil? and options[:user_buy_package] then
      options = options.merge(user_buy_package())
    else
      options = options.merge(default_options())
    end
    
    h = super(options)
    if not options[:base_info].nil? and options[:base_info] then
      
    end
    
    h[:id] = id.to_s
    h
  end

  def home_info
    {
      :format => :table,
      :only => [:m, :v, :t, :q],
      :except => ["a", "bar_ids", "bars"],
      :time_format => "%m/%d/%Y %H:%M %t",
      :date_format => "%m/%d/%Y"
    }
  end
  
  def user_buy_package
    {
      :format => :table,
      :only => [:m, :v, :t, :q],
      #:except => ["a", "bar_ids", "bars", "description"],
      :time_format => "%m/%d/%Y %H:%M %t",
      :date_format => "%m/%d/%Y"
    }
  end
  
  def user_packages_info
    {
      :format => :table,
      :only => [:m, :v, :t, :q],
      #:except => ["a", "bar_ids", "bars", "description"],
      :time_format => "%m/%d/%Y %H:%M %t",
      :date_format => "%m/%d/%Y"
    }
  end
  
  def search_package_info
    {
      :format => :table,
      :only => [:m, :v, :t, :q],
      #:except => ["a", "bar_ids", "bars", "description"],
      :time_format => "%m/%d/%Y %H:%M %t",
      :date_format => "%m/%d/%Y"
    }
  end
  
  def detail_info
    {
      :format => :table,
      :only => nil,
      :except => [:bars, "bar_ids", :active],
      :time_format => "%m/%d/%Y %H:%M %t",
      :date_format => "%m/%d/%Y"
    }
  end
  
  def base_info
    {
      :format => :table,
      :only => [:_id, :v, :dt, :rv, :d_c, :dt_c, :t, :q, :qv ],
        :include => {
            :package_items => {
                :only => [:v, :t, :q, :vol, :b]
             }
        },
        
      :except => [:bars, "bar_ids"],
      :time_format => "%m/%d/%Y %H:%M %t",
      :date_format => "%m/%d/%Y"
    }
  end
  
  def default_options
    {
      :format => :table,
      :only => nil,
      :time_format => "%m/%d/%Y %H:%M %t",
      :date_format => "%m/%d/%Y"
    }
  end
  
  def serializable_hash(options)
    original_hash = super(options)
    Hash[original_hash.map {|k, v| [self.aliased_fields.invert[k] || k , v] }]
  end
    
  def calculate_values
    ##calcula o valor do pacote
    self.sale_value = 0
    self.real_value = 0
    package_items.each do |item|
      self.sale_value += item.sale_value
      self.real_value += item.real_value
    end
  end
    
  private

    def assign_id
      self._id = Sequence.generate_id(:package)
    end

end