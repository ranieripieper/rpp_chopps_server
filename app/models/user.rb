class User
  include Mongoid::Document
  include Mongoid::Timestamps
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
       
  before_save :ensure_authentication_token
  
  field :_id,                                  type: Integer
  
  ## Database authenticatable
  field :e,     as: :email,                     type: String, default: ""
  field :p,     as: :encrypted_password,        type: String, default: ""

  ## Recoverable
  field :rpt,   as: :reset_password_token,      type: String
  field :rps,   as: :reset_password_sent_at,    type: Time
  
  ## Rememberable
  field :rca,   as: :remember_created_at,       type: Time

  ## Trackable
  field :qtL,   as: :sign_in_count,             type: Integer, default: 0
  field :cDt,   as: :current_sign_in_at,        type: Time
  field :lDt,   as: :last_sign_in_at,           type: Time
  field :cIp,   as: :current_sign_in_ip,        type: String
  field :lIp,   as: :last_sign_in_ip,           type: String
  field :at,    as: :authentication_token,      type: String
  
  ## Campos Choops
  field :n,     as: :name,                      type: String
  field :tel,   as: :phone,                     type: String
  field :fbTk,  as: :facebook_token,            type: String
  field :fbId,  as: :facebook_id,               type: String
  field :ph,    as: :photo,                     type: String
  field :lc,    as: :last_chopp,                type: String

  embeds_many :package_users

  def as_json(options = { })

  
    if not options[:base_info].nil? and options[:base_info] then
      options = options.merge(base_info())
    elsif not options[:home_info].nil? and options[:home_info] then
      options = options.merge(home_info())
    elsif not options[:update_info].nil? and options[:update_info] then
      options = options.merge(update_info())
    else
      options = options.merge(default_options())
    end
    
    h = super(options)
    
    #last chopp
    if self.lc.nil? then
      h[:last_chopp_bar] = "Bar do Juarez"
      h[:last_chopp_dt] = Time.now
    else
      h[:last_chopp_bar] = self.lc.split("||")[0]
      h[:last_chopp_dt] = self.lc.split("||")[1]
    end


    if not package_users.nil? and not options[:update_info] then
  h[:package_users] = package_users.last_update(5).as_json(options)
    elsif not package_users.nil? and not options[:update_info] then
      h[:package_users] = package_users.as_json(options)
    end
    #end
    
    if not options[:home_info].nil? and options[:home_info] then
      h[:qt_chopps] = [{"type" => 1, "qtde" => 10}, {"type" => 2, "qtde" => 8}]
    end
    h[:id] = id.to_s
    h
  end
  
  def home_info 
    {
        :only => [:id, :at, :n, :ph,:package_users, :qt_claro, :qt_escuro]
     }
  end
  
  def reg_info 
    {
        :only => [:id, :at, :n, :ph,:package_users, :qt_claro, :qt_escuro]
       # :except => [:package_users]
     }
  end

  def update_info 
    {
        :only => [:id, :n, :e, :at]
     }
  end
  
  def base_info 
    {
        :only => [:id, :n, :e,:package_users]
     }
  end
  
  def default_options
    {
      :format => :table,
      :only => nil,
      :except => [:p],
      :time_format => "%m/%d/%Y %H:%M %t",
      :date_format => "%m/%d/%Y"
    }
  end
  
  def ensure_authentication_token
    if authentication_token.blank?
      self.authentication_token = "#{self.id}:#{generate_authentication_token}"
    end
  end

  
  class << self
    
    def find_by_authentication_token(id, token)
      return User.where(:at => "#{id}:#{token}").first
    end
  
  end

  private
  
    def generate_authentication_token
      loop do
        token = Devise.friendly_token
        break token unless User.where(authentication_token: token).first
      end
    end
    
    def assign_id
      self._id = Sequence.generate_id(:user)
    end

end
