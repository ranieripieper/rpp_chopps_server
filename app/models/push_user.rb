class PushUser
  include Mongoid::Document

  ## Campos Choops
  field :t,     as: :type,                      type: Integer
  field :tk,    as: :token,                     type: String

  
  def serializable_hash(options)
      original_hash = super(options)
      Hash[original_hash.map {|k, v| [self.aliased_fields.invert[k] || k , v] }]
  end

  class << self
    def serialize_from_session(key, salt)
      record = to_adapter.get(key[0]["$oid"])
      record if record && record.authenticatable_salt == salt
    end
  end


end
