class Historical
  
  include Mongoid::Document
  include Mongoid::Timestamps

  @@BUY = 1
  @@DRINK = 2

  field :d,     as: :desc,                     type: String
  field :t,     as: :type,                     type: Integer # Tipo (1-BUY, 2-DRINK)
  
  belongs_to :bar
  belongs_to :user
  belongs_to :package_user
  
  def serializable_hash(options)
    original_hash = super(options)
    Hash[original_hash.map {|k, v| [self.aliased_fields.invert[k] || k , v] }]
  end
  
  def self.buy
    @@BUY
  end
  
  def self.drink
    @@DRINK
  end
  
end