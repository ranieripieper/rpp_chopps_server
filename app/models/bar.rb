require 'geocoder'

class Bar
  include Mongoid::Document
  include Geocoder::Model::Mongoid
  include Mongoid::Timestamps
  include Mongoid::Paperclip
  
  before_create :assign_id
  
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  before_save :before_save_bar

  field :_id,                                  type: Integer
 
  ## Database authenticatable
  field :e,     as: :email,                     type: String, default: ""
  field :p,     as: :encrypted_password,        type: String, default: ""

  ## Recoverable
  field :rpt,   as: :reset_password_token,      type: String
  field :rps,   as: :reset_password_sent_at,    type: Time
  
  ## Rememberable
  field :rca,   as: :remember_created_at,       type: Time

  ## Trackable
  field :qtL,   as: :sign_in_count,             type: Integer, default: 0
  field :cDt,   as: :current_sign_in_at,        type: Time
  field :lDt,   as: :last_sign_in_at,           type: Time
  field :cIp,   as: :current_sign_in_ip,        type: String
  field :lIp,   as: :last_sign_in_ip,           type: String
  field :at,    as: :authentication_token,      type: String
  
  ## Campos Choops
  field :n,     as: :name,                      type: String
  field :d,     as: :description,               type: String
  field :pu,    as: :pwd_unlock,                type: String, default: ""
  field :tel,   as: :phone,                     type: String
  field :a,     as: :active,                    type: Boolean, default: false
  has_and_belongs_to_many :packages
  embeds_one :address, as: :addressed
  
  has_mongoid_attached_file :logo
  validates_attachment_content_type :logo, :content_type => /\Aimage\/.*\Z/
  validates_with AttachmentSizeValidator, :attributes => :logo, :less_than => 1.megabytes
  
  field :geo_near_distance
      
  #rake db:mongoid:create_indexes
  #db.events.ensureIndex( { "address.coordinates" : "2dsphere" } )
  index({ "address.coordinates" => "2dsphere" }, { name: "coord_index", background: true })
  
  def logo_url
    logo.url
  end
  
  def as_json(options = { })
=begin

    if not options[:home_info].nil? and options[:home_info] then
      options = options.merge(home_info())
    elsif (not options[:base_info].nil? and options[:base_info]) or (not options[:bar_base_info].nil? and options[:bar_base_info]) then
      options = options.merge(base_info())
    elsif not options[:detail_info].nil? and options[:detail_info] then
      options = options.merge(detail_info())
    elsif not options[:search_package_info].nil? and options[:search_package_info] then
      options = options.merge(search_package_info())
    elsif not options[:bar_package_user].nil? and options[:bar_package_user] then
      options = options.merge(bar_package_user())
    end

    h = super(options)

    
    if not packages.nil? and not options[:search_package_info].nil? then
      tmp_pkg = packages.search_value(options[:package_value]).order_by(:v.asc)
      tmp_types = []
      result_types = []
      #Retorna só um tipo de pacote 
      if not options[:only_one_per_type].nil? then
        if not tmp_pkg.nil? then
          tmp_pkg.each do |pkg| 
              
            unless tmp_types.include?(pkg.type)
              result_types << pkg
              tmp_types << pkg.type
            end
          end
        end
      end
      h[:packages] = result_types.as_json(options)
       
    elsif not packages.nil? and (options[:home_info].nil?) and (options[:base_info].nil?) and (options[:bar_package_user].nil?) then   
      h[:packages] = packages.as_json(options)

    else
      h.delete(:packages)
    end

    if not h["package_user"].nil? then
      h["package_user"] = h["package_user"].as_json(options)
    end
  
=end    

    if (not options[:search_bar].nil? and options[:search_bar]) then
      options = options.merge(search_bar())
    end

    #verifica se inclui os pacotes
    if (not options[:include_packages].nil? and options[:include_packages]) then
      options = options.merge(include_packages())
    end
    
    h = super(options)

    #inclui pacotes
    if (not options[:include_packages].nil? and options[:include_packages]) then
      h[:packages] = packages_find([:id,:package_items, :qtde, :qv, :type, :sale_value, :real_value, :dt_sale_limit, :qt_days_consumption, :dt_consumption]).as_json(options)
      #h[:packages] = packages_find.as_json(options)
    end
    
    h[:logo_url] = logo.url

    
    
    #exclusão dos campos de logo desnecessários

    h.delete("logo_file_name")
    h.delete("logo_content_type")
    h.delete("logo_file_size")
    h.delete("logo_fingerprint")
    h.delete("logo_updated_at")
    
    #remove os parâmetros de login e desnecessários para a api
    h.delete("authentication_token")
    h.delete("current_sign_in_at")
    h.delete("current_sign_in_ip")
    h.delete("last_sign_in_at")
    h.delete("last_sign_in_ip")
    h.delete("encrypted_password")
    h.delete("pwd_unlock")
    h.delete("sign_in_count")
    h.delete("remember_created_at")
    h.delete("reset_password_sent_at")
    h.delete("reset_password_token")
    if h["geo_near_distance"].nil? then
      h.delete("geo_near_distance")
    end
    if not h["address"].nil? then
      h["address"].delete("id")
    end
   
    h
  end



  def bar_package_user 
    {
        :only => [:_id, :n, :logo, :package_user]
     }
  end
  
  def search_package_info 
    {
        :only => [:n, :logo, :address, :coordinates]
     }
  end
  
  def home_info 
    {
        :only => [:n, :logo]
     }
  end

  def detail_info 
    {
        :only => [:_id, :n, :logo, :tel, :address, :street, :number, :complement, :neighborhood, :city, :state, :coordinates]
     }
  end


  def include_packages 
    
    {
      :format => :table,
      :only => [:_id, :n, :geo_near_distance, :tel ],
        :include => {
            :pcks => {
                :only => [:qtde]
             },
             :address => {
                :only => [:street, :number, :complement, :neighborhood, :zip_code, :city, :state, :coordinates]
             }
        },
      #:except => ["a", "bar_ids", "bars", "description"],
      :time_format => "%m/%d/%Y %H:%M %t",
      :date_format => "%m/%d/%Y"
    }
  end
  

  def search_bar 
    {
        :only => [:_id, :n, :geo_near_distance],
        :include => {
            :address => {
                :only => [:coordinates]
             }
        }
     }
  end

  def before_save_bar
    
    if authentication_token.blank?
      self.authentication_token = generate_authentication_token
    end
    
    if not BCrypt::Password.valid_hash?(self.pwd_unlock)
      
      #self.pwd_unlock = BCrypt::Password.create(pwd_unlock)
      # puts ">>>>>> before_save_bar #{self.pwd_unlock}"
    end

  end
  
  public

    def packages_find(only_fields = nil)
      if only_fields.nil? then
        Package.where(:bar_id => id, :active => true, :dt_sale_limit.gte => Date.today())
      else
        Package.only(only_fields).where(:bar_id => id, :active => true, :dt_sale_limit.gte => Date.today())
      end
    end
    
  attr_accessor :distance
  class << self
  
    def find_near(near, page=nil, query=nil, only_fileds=nil)
        page = page || 1
        per_page = MainYetting.DEFAULT_REG_PER_PAGE
  
        pipeline = [
          { "$geoNear" => {
            "near" => near,
            "distanceField" => "distance",
            "distanceMultiplier" => 6378.137,
            "maxDistance" => MainYetting.DEFAULT_KM_FIND_NEAR/6378.137,
            "spherical" => true,
            "query" => query }
          }
        ]
  
        count = collection.aggregate(pipeline).count
  
        if page && per_page
          pipeline << { '$skip' => ((page.to_i - 1) * per_page) }
          pipeline << { '$limit' => per_page }
        end
  
        places_hash = collection.aggregate(pipeline)
  
        ids = places_hash.map{|e| e['_id'].to_s}
        if only_fileds.nil? then
          places = self.find(ids)
        else
          only_fileds << :geo_near_distance
          places = get_bar_search_info_by_ids(ids, only_fileds)
        end
        
  
        places = inject_distance_and_sort(places, places_hash)
  
        places.instance_eval <<-EVAL
          def current_page
            #{ page }
          end
          def total_pages
            #{ count.modulo(per_page).zero? ? (count / per_page) : ((count / per_page) + 1)}
          end
          def limit_value
            #{ per_page }
          end
        EVAL
  
        places
      end
  end 
  
  def serializable_hash(options)
    original_hash = super(options)
    Hash[original_hash.map {|k, v| [self.aliased_fields.invert[k] || k , v] }]
  end
  
  private
    # private class methods
    class << self
      def inject_distance_and_sort(places, places_hash)
        places = places.each do |place|
          p = places_hash.select {|p| p["_id"] == place.id}.first
          place.geo_near_distance =  p["distance"]
        end
  
        places.sort_by{|a| [ a.geo_near_distance]}
          
           #|a,b| b.destaque <=> a.destaque, a.geo_near_distance <=> b.geo_near_distance }
      end
      
      def get_bar_search_info_by_ids(ids, only_fileds)
        Rails.cache.fetch("#{ids}/bar_search_info", expires_in: 1.minutes) do
          Bar.only(only_fileds).find(ids)
        end
      end
    
    end

    def assign_id
      self._id = Sequence.generate_id(:bar)
    end
    
    def generate_authentication_token
      loop do
        token = Devise.friendly_token
        break token unless Bar.where(authentication_token: token).first
      end
    end
end