class PackageItem
  
  include Mongoid::Document

  field :_id,                                  type: Integer, default: nil
  field :b,     as: :brand,                    type: String # Marca de chopp/cerveja (Brahma, Skol, Itaipava...)
  field :t,     as: :type,                     type: String # Tipo de chopp/cerveja (Claro, Escuro, Ruiva...)
  field :v,     as: :sale_value,               type: Float
  field :rv,    as: :real_value,               type: Float
  field :q,     as: :qtde,                     type: Integer, default: 10
  field :vol,   as: :volume,                   type: Integer, default: 10
  
  embedded_in :package
  
  def as_json(options = { })
    h = super(options)
    
    h.delete(:sale_value)
    h.delete(:real_value)
    h.delete(:_id)
    h.delete(:id)
    
    h
  end
  
  def serializable_hash(options)
    original_hash = super(options)
    Hash[original_hash.map {|k, v| [self.aliased_fields.invert[k] || k , v] }]
  end

end