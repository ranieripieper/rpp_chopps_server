class Brand
  
  include Mongoid::Document

  field :_id,                                  type: Integer
  field :b,     as: :brand,                    type: String # Marca de chopp/cerveja (Brahma, Skol, Itaipava...)

end