class PackageUser
  
  include Mongoid::Document
  include Mongoid::Timestamps

  field :_id,                                        type: Integer
  field :dt,          as: :dt_expire,                type: DateTime
  field :dtL,         as: :dt_last_cons,             type: DateTime
  field :qtCons,      as: :qtde_cons,                type: Integer, default: 0
  field :v,           as: :valor_pgto,               type: Float
  field :c,           as: :consumed,                 type: Boolean, default: false
  
  has_many :packages
  
  embedded_in :user

  scope :search_by_bar, ->(bar_id, consumed){ where(:bar_id => bar_id, :consumed => consumed) }
  scope :search_all_by_bar, ->(bar_id){ where(:bar_id => bar_id) }
  scope :search_by_id, ->(id){ where(:id => id) }
  scope :not_consumed, ->{ where(:consumed => false) }
  scope :by_consumed, ->(consumed){ where(:consumed => consumed) }
  scope :last_update, ->(limit){ order_by(:updated_at.asc).limit(limit) }

  def as_json(options = { })
    options[:only] = nil
    if not options[:base_info].nil? and options[:base_info] then
      options = options.merge(base_info())
    elsif not options[:home_info].nil? and options[:home_info] then
      options = options.merge(home_info())
    elsif not options[:user_packages_info].nil? and options[:user_packages_info] then
      options = options.merge(user_packages_info())
    elsif not options[:user_buy_package].nil? and options[:user_buy_package] then
      options = options.merge(user_buy_package())
    end

    h = super(options)
  
    if not options[:home_info].nil? and options[:home_info] then
      h[:package_bars] = package_bar(options).as_json(options)
      h.delete("valor_pgto")
    end
   
    if (not options[:user_packages_info].nil? and options[:user_packages_info]) or (not options[:user_buy_package].nil? and options[:user_buy_package]) then
      h[:package] = package_bar(options).as_json(options)
    else 
      h[:bar] = bar(options).as_json(options)
    end
    
    h[:id] = id.to_s
    
    h
  end


  def user_buy_package 
    {
        :only => [:id, :dt, :qtCons, :bar, :package],
        :except => [:valor_pgto, :bar_package_id, :bar_id, :expire]
     }
  end
  
  def user_packages_info 
    {
        :only => [:id, :dt, :qtCons, :package, :c],
        :except => [:valor_pgto, :bar_package_id, :expire, :bar, :bar_id, :bar_package_id]
     }
  end
  
  def home_info 
    {
        :only => [:id, :dt, :qtCons, :bar, :package],
        :except => [:valor_pgto]
     }
  end
  
  def base_info 
    {
        :only => [:id, :dt, :qtCons, :v, :bar]
     }
  end


  def bar(options)
    if not options.nil? and not options[:home_info].nil? and options[:home_info] then
      Bar.only(:_id, :name).find(self.bar_id)
    else
      Bar.find(self.bar_id)
    end
  end
  
  def package_bar(options)
    if not options.nil? and not options[:home_info].nil? then
      PackageBar.only(:_id, :mark, :type, :qtde).find(self.bar_package_id)
    else
      PackageBar.find(self.bar_package_id)
    end
  end
   
  def serializable_hash(options)
    original_hash = super(options)
    Hash[original_hash.map {|k, v| [self.aliased_fields.invert[k] || k , v] }]
  end
  
  private
    
    def assign_id
      self._id = Sequence.generate_id(:pck_user)
    end
end