class AdminController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.

  #before_action :configure_permitted_parameters, if: :devise_controller?
  
  before_action :configure_devise_permitted_parameters, if: :devise_controller?

  #layout :layout_by_resource
  
  protected


  def layout_by_resource
    #puts "$$$$$$$$$$$ #{resource_name}"
    #if devise_controller? && resource_name == :bar
    #  "layout_name_for_devise_admin"
    #else
    #  "application"
    #end
  end

  def configure_devise_permitted_parameters
    registration_params = [:name, :email, :password, :password_confirmation, :description, :pwd_unlock]

    if params[:action] == 'update'
      devise_parameter_sanitizer.for(:account_update) { 
        |u| u.permit(registration_params << :current_password)
      }
    elsif params[:action] == 'create'
      devise_parameter_sanitizer.for(:sign_up) { 
        |u| u.permit(registration_params) 
      }
    end
  end


  
  # For this example, we are simply using token authentication
  # via parameters. However, anyone could use Rails's token
  # authentication features to get the token from a header.
  def authenticate_user_from_token!
    
    user_token = "#{request.headers['User-Token']}"
    

    if user_token.nil? or user_token.empty? then
      user_token = params['User-Token'].presence
    end
    
    #user_token = params[:user_token].presence
    if user_token then
      pos_id = user_token.index(":")
      id = user_token[0..pos_id-1]
      token = user_token[pos_id+1..user_token.size]
      user = User.find_by_authentication_token(id, token)
    end
   
 
    if user
      # Notice we are passing store false, so the user is not
      # actually stored in the session and a token is needed
      # for every request. If you want the token to work as a
      # sign in token, you can simply remove store: false.
      sign_in user, store: false
    end
  end
end
