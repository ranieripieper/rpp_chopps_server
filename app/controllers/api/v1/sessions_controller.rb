# file: app/controller/api/v1/sessions_controller.rb
class Api::V1::SessionsController < Devise::SessionsController
  include Chopps::UserHelpers
  
  skip_before_filter :verify_authenticity_token,
                     :if => Proc.new { |c| c.request.format == 'application/json' }

  respond_to :json

  ## 
  ## curl -H 'Content-Type: application/json' -H 'Accept: application/json' -X POST http://choppsdds.dev.com/api/v1/sessions -d "{\"user\":{\"email\":\"ranieripieper@gmail.com\",\"password\":\"secret123\"}, \"lng\": -46.6688706, \"lat\":-23.6001609}"
  ##
  def create
    resource = warden.authenticate!(auth_options)
    warden.authenticate!(:scope => resource_name, :recall => "#{controller_path}#failure")
    
    current_user.save
    
    get_user(current_user, params)
    
  end

  #curl -v -H 'Content-Type: application/json' -H 'Accept: application/json' -X DELETE http://localhost:3000/api/v1/sessions/\?auth_token\=GPtR4Q5eVNf6-auLqGsV
  def logout
    warden.authenticate!(:scope => resource_name, :recall => "#{controller_path}#failure")
    current_user.update_column(:authentication_token, nil)
    render :status => 200,
           :json => { :success => true,
                      :info => "Logged out",
                      :data => {} }
  end

  def failure
    render :status => 401,
           :json => { :success => false,
                      :info => "Login Failed",
                      :data => {} }
  end
end