class Api::V1::BarsController < ApplicationController
  include Chopps::UserHelpers
  respond_to :xml, :json
  
  #before_action :authenticate_user!
  #before_filter :authenticate_user_from_token!
  
  #Retorna dados do bar
  # http://choppsdds.dev.com/api/v1/bar/545a5e0c52616e29a8010000.json
  def get
    bar_id = params[:bar_id]
    if bar_id.nil? or bar_id.empty? then
        respond_to do |format|
          format.xml  { render :xml => {:success => false,
                                        :info => "Parâmetros inválidos!"} }
          format.json { render :json => {:success => false,
                                         :info => "Parâmetros inválidos!"} }
        end
    else
      bar = Bar.find(bar_id)

      #verifica se o usuário tem pacote no bar
      user_has_pkg = false
      if not current_user.nil? and not current_user.package_users.nil? then
        user_has_pkg = user_has_pkg(current_user, bar.id)
      end
      
      respond_to do |format|
        format.json { render :json => {:success => true,
                                       :bar => bar.as_json(:include_packages =>  true ), 
                                       :user_has_pkg => user_has_pkg}
                                       }
      end
    end
  end
  
    
  #Retorna os bares com os pacotes próximos
  # curl http://choppsdds.herokuapp.com/api/v1/nr_bars.json
  def count
    respond_to do |format|
      format.xml  { render :xml => {:success => true,
                                    :nr => Bar.count} }
      format.json { render :json => {:success => true,
                                     :nr => Bar.count} }
    end

  end

  #Retorna os bares com os pacotes próximos
  # curl http://choppsdds.herokuapp.com/api/v1/bars.json?lat=-23.56033&lng=-46.58301&search=xxx
  def find_near

    lat = params[:lat]
    lng = params[:lng]
    search = params[:search]
    page = 1
    
    if params[:page].nil? then 
      page = 1
    else
      page = params[:page].to_i
    end
    
    if (lat.nil? or lng.nil?) and search.nil? then
        respond_to do |format|
          format.xml  { render :xml => {:success => false,
                                        :info => "Parâmetros inválidos"} }
          format.json { render :json => {:success => false,
                                         :info => "Parâmetros inválidos"} }
        end
    else 
      bars = nil
      only_fields = [:id, :name, :logo_file_name, :logo_updated_at, "address.coordinates"]
       
      if search.nil? then
        
        @location = [lng.to_f, lat.to_f]
        bars = Bar.find_near( [lng.to_f, lat.to_f], page, nil, only_fields)

      else
        #remove acentos
        search = ActiveSupport::Inflector.transliterate(search)
        search = make_search_exp(search)

        query = []
        query.push({"n" => /.*#{search}.*/i })
        queryJson = { "$and" => query }
          
        if lat.nil? or lng.nil? then
          per_page = MainYetting.DEFAULT_REG_PER_PAGE
          bars = Bar.where(queryJson).limit(per_page).skip(((page - 1) * per_page))
        else
          bars = Bar.find_near( [lng.to_f, lat.to_f], page, queryJson, only_fields) 
        end
      end
      respond_to do |format|
        format.json { render :json => {:success => true, :bars => bars.as_json(:search_bar => true)}}
      end
    end
  end
  
  def make_search_exp(str)
     n = str.downcase.strip.to_s
     n.gsub!("a",    '[aàãá]')
     n.gsub!("e",    '[eéê]')
     n.gsub!("i",    '[ií]')
     n.gsub!("o",    '[oó]')
     n.gsub!("u",    '[uú]')
     n.gsub!("c",    '[cç]')
     n
  end
  
end
