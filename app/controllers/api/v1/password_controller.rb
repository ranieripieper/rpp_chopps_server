class Api::V1::PasswordController < Devise::SessionsController
  skip_before_filter :verify_authenticity_token, :if => Proc.new { |c| c.request.format == 'application/json' }
  respond_to :json

  ## Comando de teset para resetar a senha
  ## curl -H 'Content-Type: application/json' -H 'Accept: application/json' -X POST http://choppsdds.herokuapp.com/api/v1/reset_password.json -d "{\"user\":{\"email\":\"ranieripieper@gmail.com\"}}"

  def create
    @user = User.send_reset_password_instructions(params[:user])
    if successfully_sent?(@user)
      render :status => 200, :json => {:success => true}
    else
      render :status => 422, :json => {:success => false,  :errors => @user.errors.full_messages }
    end
  end
end