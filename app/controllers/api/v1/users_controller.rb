class Api::V1::UsersController < ApplicationController
  include Chopps::UserHelpers
  
  respond_to :xml, :json
  
  rescue_from Mongoid::Errors::DocumentNotFound, :with => :render_not_found

  before_filter :authenticate_user_from_token!

  #before_action :authenticate_user!, :with => :render_not_found

  #Retorna dados dos pacotes do usuário para tomar um chopp
  # curl --cookie cookie2.txt "http://choppsdds.dev.com/api/v1/user/drink_chopp.json?package_user=545a7af752616e2e2b000000&pwd_unlock=teste123"
  def drink_chopp
    pkg_user_id = params[:package_user]
    pwd_unlock = params[:pwd_unlock]

    if not user_signed_in? then
        respond_to do |format|
          format.xml  { render :xml => {:success => false,
                                        :info => "Usuário não autenticado!"} }
          format.json { render :json => {:success => false,
                                         :info => "Usuário não autenticado!"} }
        end
    elsif pkg_user_id.nil? or pwd_unlock.nil? then
        respond_to do |format|
          format.xml  { render :xml => {:success => false,
                                        :info => "Parâmetros inválidos!"} }
          format.json { render :json => {:success => false,
                                         :info => "Parâmetros inválidos!"} }
        end
    else
      msg_error = ""
      #verifica senha do bar       
      pkg_user = current_user.package_users.search_by_id(pkg_user_id).first
      if pkg_user.nil? then
        msg_error = "Pacote não encontrado!"
      else
        bar = pkg_user.bar(nil)
        if bar.nil? then
          msg_error = "Bar não encontrado!"
        else
          ##Verifica senha

          pwd_unlock_uncrypt = bar.pwd_unlock#BCrypt::Password.new(bar.pwd_unlock)

          if pwd_unlock != pwd_unlock_uncrypt then
            msg_error = "Senha não confere! #{bar.name} #{pwd_unlock_uncrypt} - #{bar.pwd_unlock}"
          else
             #msg_error = "Senha  confere! #{bar.name} #{pwd_unlock_uncrypt} - #{bar.pwd_unlock}"
             #veririca se ainda tem chopp no pacote
             pkg_bar = pkg_user.package_bar(nil)
             if pkg_user.qtde_cons >= pkg_bar.qtde then
               msg_error = "O pacote já foi consumido!"
             else
                #atualiza dados de consumo
                consumed = false
                if pkg_user.qtde_cons+1 == pkg_bar.qtde
                  consumed = true
                end
                pkg_user.set(qtde_cons: pkg_user.qtde_cons+1, consumed: consumed)
             end
          end
        end
      end
      
      
      #retorna
      
      if msg_error.nil? or msg_error.empty? then
        
        #Salva histórico
        historical = Historical.new
        historical.type = Historical.drink
        historical.desc = "Drink"
        historical.user = current_user
        historical.package_user = pkg_user
        historical.save!
        
        respond_to do |format|
          format.xml  { render :xml => {:success => true,
                                        :package => pkg_user} }
          format.json { render :json => {:success => true,
                                         :package => pkg_user.as_json( 
                                             :user_packages_info =>  true)}
                                         }
        end
      else 
        respond_to do |format|
          format.xml  { render :xml => {:success => false,
                                        :info => msg_error} }
          format.json { render :json => {:success => false,
                                         :info => msg_error} }
        end
      end
    end
  end
  
  #Retorna dados dos pacotes do usuário para tomar um chopp
  # curl --cookie cookie2.txt "http://choppsdds.dev.com/api/v1/user/my_packages.json"
  # curl --cookie cookie2.txt "http://choppsdds.dev.com/api/v1/user/my_packages.json?bar_id=545a5e0c52616e29a8010000"
  def my_packages
    #current_user = User.find("54539b6c3933640002000000")
    
    if not user_signed_in? then
        respond_to do |format|
          format.xml  { render :xml => {:success => false,
                                        :info => "Usuário não autenticado!"} }
          format.json { render :json => {:success => false,
                                         :info => "Usuário não autenticado!"} }
        end
    else
      pkgs = nil
      if not params[:bar_id].nil? then
        pkgs = current_user.package_users.search_all_by_bar(params[:bar_id]).order_by(:dt_expire.asc)
      else
        if not params[:only_active].nil? and params[:only_active].to_b then
          pkgs = current_user.package_users.by_consumed(false).order_by(:dt_expire.asc)
        else
          pkgs = current_user.package_users.order_by(:dt_expire.asc)#.group_by {|d| ("bar_id:#{d.bar(nil)._id}") }
        end
      end

      bars = []
      bars_id = []
      pkgs.each do |pkg|
        bar = nil
        if not bars_id.include?(pkg.bar_id) then
          bar = Bar.find(pkg.bar_id)
          bars << bar
          bars_id << pkg.bar_id
        else
         bars.each do |b|
           if b._id == pkg.bar_id then
             bar = b
             break
           end
         end
        end
        
        if not bar.nil? then
          if bar["package_user"].nil? then
            bar["package_user"] = []
          end
          bar["package_user"] << pkg
        else
        end
      end
      
      respond_to do |format|
        format.json { render :json => {:success => true,
                                       :bars => bars.as_json( 
                                           :user_packages_info => true,
                                           :bar_package_user => true)}
                                       }
      end
    end
  end
  
  #Retorna dados dos pacotes do usuário para tomar um chopp
  # curl --cookie cookie2.txt "http://choppsdds.dev.com/api/v1/user/my_packages.json"
  # curl --cookie cookie2.txt "http://choppsdds.dev.com/api/v1/user/my_packages.json?bar_id=545a5e0c52616e29a8010000"
  def my_packages_old
    #current_user = User.find("54539b6c3933640002000000")
    
    if not user_signed_in? then
        respond_to do |format|
          format.xml  { render :xml => {:success => false,
                                        :info => "Usuário não autenticado!"} }
          format.json { render :json => {:success => false,
                                         :info => "Usuário não autenticado!"} }
        end
    else
      pkgs = nil
      if not params[:bar_id].nil? then
        pkgs = current_user.package_users.search_all_by_bar(params[:bar_id]).order_by(:dt_expire.asc)
      else
        if not params[:only_active].nil? and params[:only_active].to_b then
          pkgs = current_user.package_users.by_consumed(false).order_by(:dt_expire.asc)
        else
          pkgs = current_user.package_users.order_by(:dt_expire.asc)
        end
      end
      
      respond_to do |format|
        format.xml  { render :xml => {:success => true,
                                      :packages => pkgs} }
        format.json { render :json => {:success => true,
                                       :packages => pkgs.as_json( 
                                           :user_packages_info => true,
                                           :bar_base_info => true)}
                                       }
      end
    end
  end
  
  
  #Retorna dados dos pacotes do usuário para tomar um chopp
  # curl --cookie cookie2.txt "http://choppsdds.dev.com/api/v1/user/my_packages_not_consumed.json"
  # curl --cookie cookie2.txt "http://choppsdds.dev.com/api/v1/user/my_packages_not_consumed.json?bar_id=545a5e0c52616e29a8010000"
  def my_packages_not_consumed
    # para debug current_user = User.find("54539b6c3933640002000000")
    if not user_signed_in? then
        respond_to do |format|
          format.xml  { render :xml => {:success => false,
                                        :info => "Usuário não autenticado!"} }
          format.json { render :json => {:success => false,
                                         :info => "Usuário não autenticado!"} }
        end
    else
      pkgs = nil
      if not params[:bar_id].nil? then
        pkgs = current_user.package_users.search_by_bar(params[:bar_id], false).order_by(:dt_expire.asc)
      else
        pkgs = current_user.package_users.not_consumed.order_by(:dt_expire.asc)
      end
      
      respond_to do |format|
        format.xml  { render :xml => {:success => true,
                                      :packages => pkgs} }
        format.json { render :json => {:success => true,
                                       :packages => pkgs.as_json( 
                                           :user_packages_info =>  true)}
                                       }
      end
    end
  end
  
  #Retorna dados do usuário
  # curl -H 'Content-Type: application/json' -H 'Accept: application/json' -X POST http://choppsdds.dev.com/api/v1/sessions -d "{\"user\":{\"email\":\"ranieripieper@gmail.com\",\"password\":\"teste123\"}}" --cookie-jar cookie2.txt
  # curl --cookie cookie2.txt "http://choppsdds.dev.com/api/v1/user.json?lat=-23.6001609&lng=-46.6688706"
  def get

    if not user_signed_in? then
        respond_to do |format|
          format.xml  { render :xml => {:success => false,
                                        :info => "Usuário não autenticado"} }
          format.json { render :json => {:success => false,
                                         :info => "Usuário não autenticado"} }
        end
    else
      get_user(current_user, params)
    end
  end
  
  # Realiza compra de pacotes
  # curl  "http://choppsdds.herokuapp.com/api/v1/buy_package.json?bar_id=5458b5e652616e1c8a010000&package_id=5458b93c52616e1d21000000"
  
  #curl --cookie cookie2.txt  -H 'Content-Type: application/json' -H 'Accept: application/json' -X POST http://choppsdds.dev.com/api/v1/buy_package.json -d "{ \"bar_id\": \"545a6f2752616e2c33070000\", \"package_id\":\"545a6fad52616e2c330d0000\"}"  --cookie-jar cookie2.txt


  def buy_package
    #post recebe Id do usuário, id do pacote, id do bar
    package_id = params[:package_id]
    bar_id = params[:bar_id]

    bar = nil
    package = nil
    
    ##valida parâmetros
    if not user_signed_in? or current_user.nil? then
      json_error = "Acesso negado!"
    elsif package_id.nil? or package_id.empty? or 
      bar_id.nil? or bar_id.empty? then
      json_error = "Parâmetros inválidos!"
    else 
      
      bar = Bar.find(bar_id)
      json_error = "";
      if bar.nil? then
        json_error = "Bar #{bar_id} não encontrado!"
      else
        findPackage = false
        bar.package_bars.each do |pck|
          if (package_id == pck._id.to_s)  then
            if pck.active then
              findPackage = true
              package = pck
              break
            end
            
          end
        end
        if not findPackage then
          json_error = "Pacote #{package_id} não encontrado!"
        end
      end
    end

    if not json_error.empty? then
        respond_to do |format|
          format.xml  { render :xml => {:success => false,
                                        :info => json_error} }
          format.json { render :json => {:success => false,
                                         :info => json_error} }
        end
    else

      ##Realiza processo de pgto
      ## TODO
      
      ##Realiza cálculo da data de expiração 
      pkg_user = PackageUser.new
      pkg_user.bar_id = bar._id
      pkg_user.bar_package_id = package._id
      pkg_user.dt = Time.now + 10.days
      if (current_user.package_users.nil?) then
        current_user.package_users = []
      end
      current_user.package_users.push(pkg_user)
      current_user.save!
      
      pkg_user.save!

      #Salva histórico
      historical = Historical.new
      historical.type = Historical.buy
      historical.desc = "Compra"
      historical.user = current_user
      historical.package_user = pkg_user
      historical.save!
        
      respond_to do |format|
        format.xml  { render :xml => {:success => true,
                                      :package => pkg_user} }
        format.json { render :json => {:success => true,
                                       :package => pkg_user.as_json( 
                                           :user_buy_package =>  true)} }
      end
        
    
    end
  end
    
  def render_not_found(exception = nil)
    respond_to do |format|
      format.xml  { render :xml => {:success => false,
                                    :info => "#{exception.message}"} }
      format.json { render :json => {:success => false,
                                     :info => "#{exception.message}"} }
    end
  end
    
  
end
