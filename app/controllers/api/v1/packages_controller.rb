class Api::V1::PackagesController < ApplicationController
  respond_to :json
  
  rescue_from Mongoid::Errors::DocumentNotFound, :with => :render_not_found

  #before_action :authenticate_user!

  #Retorna dados do usuário
  #curl http://choppsdds.herokuapp.com/api/v1/packages/search.json
  def search_packages
    value = params[:value]
    type = params[:type]
    
    if value.nil? or value.empty? or type.nil? or type.empty? then
        respond_to do |format|
          format.json { render :json => {:success => false,
                                         :info => "Parâmetros inválidos!"} }
        end
    else
      #bars = Bar.where(:t => type.to_i, :v.gte => value).order_by(:v.asc)
      packages = Package.only(:id, :bar).where(:t => type.to_i, :v.lte => value).order_by(:v.asc)
      bars = []
      if not packages.nil? then
        #busca os bares com os pacotes
        packages.each do |pkg| 
          if not pkg.bar.nil? then
            unless bars.include?(pkg.bar)
              bars << pkg.bar
            end
          end
        end
       
      end
      #packages = PackageBar.where(:t => type.to_i, :v.gte => value).order_by(:v.asc)
      respond_to do |format|
        format.json { render :json => {:success => true,
                                       :data => bars.as_json( 
                                           :search_package_info =>  true,
                                           :package_value =>  value,
                                           :only_one_per_type => true)}         
                    }
      end
    end
    
  end

    
  def render_not_found(exception = nil)
    respond_to do |format|
      format.json { render :json => {:success => false,
                                     :info => "#{exception.message}"} }
    end
  end
    
  
end
