# file: app/controllers/api/v1/registrations_controller.rb
class Api::V1::RegistrationsController < Devise::RegistrationsController
  include Chopps::UserHelpers
  
  skip_before_filter :verify_authenticity_token,
                     :if => Proc.new { |c| c.request.format == 'application/json' }

  after_filter :set_csrf_header, only: [:update, :create]

  #prepend_before_filter :authenticate_scope!, only: [:update]

  respond_to :json


  ## Comando de teset para criar o usuário
  ## curl -F "user[photo]=@teste.png" -F "lng=-46.6688706" -F "lat=-23.6001609" -F "user[name]=nome usuário" -F "user[email]=user11@example.com" -F "user[password]=secret123" -F "user[password_confirmation]=secret123" -F "user[facebook_token]=fbtoken" -F "user[facebook_id]=123" http://localhost:3000/api/v1/registrations

  def create

    build_resource(sign_up_params) 
    
    new_user = resource
    photo = params[:user][:photo]
    file_name = nil

    ###verifica se tem facebookID
    if not new_user.nil? and (new_user.fbId.nil? or new_user.fbId.empty?) then
      if new_user.save then
        save = true
      end
    else
      ### verifica se tem o registro para realizar o update
      user = User.where(:fbId => new_user.fbId)
      if user.nil? then
        if new_user.save then
          save = true
        end
      else
        ##faz update do usuário
        user.name = new_user.name
        user.phone = new_user.phone
        user.email = new_user.email
        if user.update then
          save = true
          new_user = user
        end
      end
      
    end
    if save then
      if not photo.nil? then
        file_name = get_photo_filename(photo.original_filename)
        #photo = photo.auto_orient
        File.open(Rails.root.join('public', 'uploads', 'photo', file_name), 'wb') do |file|
          file.write(photo.read)
        end
         
        new_user.photo = MainYetting.HOST + "/uploads/photo/" + file_name  
   
      end
      
      sign_in new_user
      
      get_user(new_user, params)
      
    else
      render :status => :unprocessable_entity,
             :json => { :success => false,
                        :info => new_user.errors}
    end
  end

  # curl -F "user[name]=Ranieri update"  -F "user[password]=secret123" -F "user[password_confirmation]=secret123" -X PUT http://choppsdds.dev.com/api/v1/registrations   --cookie-jar cookie2.txt
  def update
    # required for settings form to submit when password is left blank
    if account_update_params[:password].blank?
      [:password,:password_confirmation,:current_password].collect{|p| account_update_params.delete(p) }
    end
    if account_update_params[:password].present? and account_update_params[:current_password].blank?
      [:current_password].collect{|p| account_update_params.delete(p) }
    end

    self.resource = resource_class.to_adapter.get!(send(:"current_#{resource_name}").to_key)
    prev_unconfirmed_email = resource.unconfirmed_email if resource.respond_to?(:unconfirmed_email)

    if update_resource(resource, account_update_params)
      yield resource if block_given?
      if is_flashing_format?
        flash_key = update_needs_confirmation?(resource, prev_unconfirmed_email) ?
          :update_needs_confirmation : :updated
        set_flash_message :notice, flash_key
      end
      
      sign_in resource_name, resource, bypass: true

      render :status => 200,
             :json => { :success => true,
                        :info => "Updated",
      
                        :data => { :user => resource.as_json(update_info: true)
                                   } }

    else
      clean_up_passwords resource
      render :status => :unprocessable_entity,
             :json => { :success => false,
                        :info => resource.errors }
    end
  end

  protected

  def update_resource(resource, params)
    resource.update_attributes(params)
  end
  
  
  def sign_up_params
    params.require(:user).permit(:email, :password, :photo, :phone, :password_confirmation, :name, :facebook_token, :facebook_id)
  end

  def account_update_params
    params.require(:user).permit(:password, :phone, :photo, :password_confirmation, :name)
  end
  
  private
    def get_photo_filename(file_name)
      #puts "****** get_photo_filename #{file_name}"
      extn = File.extname  file_name 
      #puts "****** get_photo_filename extn #{extn}"
      return SecureRandom.uuid + extn
    end
    
protected

def set_csrf_header
   response.headers['X-CSRF-Token'] = form_authenticity_token
end
end