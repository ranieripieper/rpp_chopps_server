class Admin::AdminController < AdminController
  
  def index
    if not bar_signed_in? then
      redirect_to new_bar_session_path
    end
  end
  
end
