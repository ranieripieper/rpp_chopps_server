class Admin::BarsController < AdminController
include BCrypt

  #before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :authenticate_bar!, except: [:index, :new]
  
  protect_from_forgery with: :exception

  def index
    puts "**************"
  end
  
  def show
    @bar = Bar.find params[:id]
  end

  def edit
    @bar = Bar.find params[:id]
  end
  
  def create_package

  end
  
  protected


  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up) << :name
  end
  
  def devise_parameter_sanitizer
    if resource_class == User
      devise_parameter_sanitizer.for(:sign_up) << :name
    else
      super # Use the default one
    end
  end

  
end

