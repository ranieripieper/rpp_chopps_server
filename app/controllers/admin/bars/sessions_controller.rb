class Admin::Bars::SessionsController < Devise::SessionsController
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.

  
  def after_sign_in_path_for(resource_or_scope)
    "/admin"
  end
  
  def after_sign_out_path_for(resource_or_scope)
    "/admin"
  end
end
