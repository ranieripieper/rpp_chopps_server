# file: app/controllers/api/v1/registrations_controller.rb
class Admin::Bars::RegistrationsController < Devise::RegistrationsController
 
 layout :layout_by_resource
 
  def layout_by_resource
    "admin"
  end
  
  def create
    super

    uploaded_io = params[:bar][:logo]

    file_name = nil
    if not uploaded_io.nil? then
      file_name = get_photo_filename(uploaded_io.original_filename)     
    end
    
    if not uploaded_io.nil? then
      File.open(Rails.root.join('public', 'uploads', "bar/#{file_name}"), 'wb') do |file|
        file.write(uploaded_io.read)
      end
      resource.logo = "#{MainYetting.HOST}/uploads/bar/#{file_name}"      
    end

    resource.save
  end
 
  def update
    super
    
    address = Address.new(street: params[:bar][:address][:street],
      number: params[:bar][:address][:number],
      complement: params[:bar][:address][:complement],
      neighborhood: params[:bar][:address][:neighborhood],
      zip_code: params[:bar][:address][:zip_code],
      city: params[:bar][:address][:city],
      state: params[:bar][:address][:state],
    )
    resource.address = address
    
    #uploaded_io = params[:bar][:logo]
    
    #file_name = nil
    #if not uploaded_io.nil? then
    #  file_name = get_photo_filename(uploaded_io.original_filename)     
    #end
    
    #if not uploaded_io.nil? then
    #  File.open(Rails.root.join('public', 'uploads', "bar/#{file_name}"), 'wb') do |file|
    #    file.write(uploaded_io.read)
    #  end
    #  resource.logo = "#{MainYetting.HOST}/uploads/bar/#{file_name}"      
    #end

    resource.save
  end
  
  private
 
  def sign_up_params
    params.require(:bar).permit(:name, :description, :logo, :phone, :email, :password, :password_confirmation, :pwd_unlock, 
    address: [:street, :number, :complement, :neighborhood, :zip_code, :city, :state])
  end
 
  def account_update_params
    params.require(:bar).permit(:name, :description, :logo, :phone, :email, :password, :password_confirmation, :current_password, :pwd_unlock, 
    address: [:street, :number, :complement, :neighborhood, :zip_code, :city, :state])
  end

  private
    def get_photo_filename(file_name)
      extn = File.extname  file_name 
      return SecureRandom.uuid + extn
    end
end