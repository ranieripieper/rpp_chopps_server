class Admin::Bars::PackageController < AdminController

  before_action :authenticate_bar!

  def index
    @packages = Package.admin_scope.where(:bar_id => current_bar.id)
    #@package = current_user.package_bar.build
    puts "********* index index"
  end
  
  def show
    @package = Package.find params[:id]
  end
  
  def new
    @package = Package.new
    #@package_items =[]
    
    #@package.package_item = []
    puts "*******************"
    puts current_bar
    puts "*******************"
    #render "create"
  end
  
  def create
    @package = Package.new(package_bar_params)
    @package.bar = current_bar
    
    @package.save!
    
    redirect_to admin_bar_package_index_path(current_bar)
  end

  def edit
    @package = Package.find params[:id]
    
     render "create"
  end
  
  def update
    @package = Package.find params[:id]
    @package.update_attributes package_bar_params
    
    uploaded_io = params[:package_bar][:datafile]
    if not uploaded_io.nil? then
      File.open(Rails.root.join('public', 'uploads', uploaded_io.original_filename), 'wb') do |file|
        file.write(uploaded_io.read)
      end
    end
    
    render "create"
  end


  def destroy
    @package = Package.find params[:id]
    @package.destroy
    
    render :template => 'admin/bars/index'
  end
  
  def package_bar_params
    params.require(:package).permit(:brand, :sale_value, :real_value, :qtde, :dt_consumption, :qt_days_consumption, :type, :qtde_vend, :dt_sale_limit, :active,
    :package_items => [:brand, :type, :qtde, :sale_value, :real_value, :volume])
  end
  
end
