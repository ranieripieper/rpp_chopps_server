class Admin::Bars::PackageBarsController < ApplicationController

  before_action :authenticate_bar!

  def show
    @package_bars = PackageBar.find params[:id]
  end
  
  def new
    @package_bars = PackageBar.new
    puts "*******************"
    puts current_bar
    puts "*******************"
    render "create"
  end
  
  def create
    @package_bars = PackageBar.new(package_bar_params)
    current_bar.package_bars << @package_bars
    
    uploaded_io = params[:package_bar][:datafile]
    if not uploaded_io.nil? then
      File.open(Rails.root.join('public', 'uploads', uploaded_io.original_filename), 'wb') do |file|
        file.write(uploaded_io.read)
      end
    end
    
    current_bar.save!
  end

  def edit
    @package_bars = PackageBar.find params[:id]
    
     render "create"
  end
  
  def update
    @package_bars = PackageBar.find params[:id]
    @package_bars.update_attributes package_bar_params
    
    uploaded_io = params[:package_bar][:datafile]
    if not uploaded_io.nil? then
      File.open(Rails.root.join('public', 'uploads', uploaded_io.original_filename), 'wb') do |file|
        file.write(uploaded_io.read)
      end
    end
    
    render "create"
  end


  def destroy
    @package_bars = PackageBar.find params[:id]
    @package_bars.destroy
    
    render :template => 'admin/bars/index'
  end
  
  def package_bar_params
    params.require(:package_bar).permit(:mark, :value, :original_value, :qtde, :expire, :type)
  end

  
end
