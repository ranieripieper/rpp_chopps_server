Rails.application.configure do
  
  Cieloz::Configuracao.credenciais.tap do |c|
    c.numero  = "1001734898"
    c.chave   = "e84827130b9837473681c2787007da5914d6359947015a5cdb2b8843db0fa832"
  end
  
  # Settings specified here will take precedence over those in config/application.rb.

  # In the development environment your application's code is reloaded on
  # every request. This slows down response time but is perfect for development
  # since you don't have to restart the web server when you make code changes.
  config.cache_classes = false

  # Do not eager load code on boot.
  config.eager_load = false

  # Show full error reports and disable caching.
  config.consider_all_requests_local       = true
  config.action_controller.perform_caching = false

  # Print deprecation notices to the Rails logger.
  config.active_support.deprecation = :log

  # Debug mode disables concatenation and preprocessing of assets.
  # This option may cause significant delays in view rendering with a large
  # number of complex assets.
  config.assets.debug = true

  # Adds additional error checking when serving assets at runtime.
  # Checks for improperly declared sprockets dependencies.
  # Raises helpful error messages.
  config.assets.raise_runtime_errors = true

  # Raises error for missing translations
  # config.action_view.raise_on_missing_translations = true
  
  
  config.action_mailer.delivery_method = :smtp
  config.action_mailer.perform_deliveries = true
  config.action_mailer.raise_delivery_errors = true
  config.action_mailer.default :charset => "utf-8"

  config.action_mailer.smtp_settings = {
    address: "smtp.gmail.com",
    port: 587,
    #domain: ENV["GMAIL_DOMAIN"],
    authentication: "plain",
    enable_starttls_auto: true,
    user_name: "smtp227@gmail.com ",
    password: "smtp@2014"
  }
  
  config.action_mailer.default_url_options = { :host => 'localhost' }

  config.paperclip_defaults = {
    :storage => :s3,
    :s3_credentials => {
      :bucket => "testes-dds", 
      :access_key_id => "AKIAIDTKLIO5HOIUWXNQ", 
      :secret_access_key => "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", 
    }
  }
  
  
end
