Rails.application.routes.draw do


  root :to => 'site/site#index'

  
  namespace :admin do
    resources :bars, path: '/bars', only: [:show, :edit, :update] do
      resources :package, path: '/package', module: 'bars'
    end

    
    root :to => 'admin#index'
  end
  
  devise_for :bars, path: "admin/auth_bar", 
    controllers: { sessions: "admin/bars/sessions", registrations: 'admin/bars/registrations' },   
    path_names: { sign_in: 'login', sign_out: 'logout', password: 'secret', confirmation: 'verification', 
        unlock: 'unlock', registration: 'register', sign_up: 'sign_up' } do
    
    authenticated :bar do
      root 'admin/bars#index', as: :authenticated_root #'admin#index', as: :authenticated_root
    end
  
    unauthenticated do
      root 'admin/bars/sessions#new', as: :unauthenticated_root
    end
  end

  devise_for :users
  
  namespace :api do
    namespace :v1 do
      devise_scope :user do
        post 'registrations'  => 'registrations#create',  :as => 'register'
        put  'registrations'  => 'registrations#update',  :as => 'update'
        post 'reset_password' => 'password#create',       :as => 'recoverable'
        post 'sessions'       => 'sessions#create',       :as => 'login'
        delete 'sessions'     => 'sessions#logout',       :as => 'logout'
      end
     
      get  'bar/:bar_id'                   => 'bars#get' 
      get  'bars'                          => 'bars#find_near'
      get  'nr_bars'                       => 'bars#count'
      post 'buy_package'                   => 'users#buy_package'
      get  'user'                          => 'users#get'
      get  'user/my_packages_not_consumed' => 'users#my_packages_not_consumed'
      get  'user/my_packages'              => 'users#my_packages'
      post 'user/drink_chopp'              => 'users#drink_chopp'
      get  'packages/search'               => 'packages#search_packages'
      
    end
  end

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
